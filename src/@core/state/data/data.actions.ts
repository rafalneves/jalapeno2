import { createAction, props } from '@ngrx/store';

enum Types {
  CHANGE_SELECTION = '@data/CHANGE_SELECTION',
}

export const changeSelection = createAction(Types.CHANGE_SELECTION, props<{ value: string | number | undefined }>());
