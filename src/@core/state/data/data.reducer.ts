import { createReducer, on } from '@ngrx/store';

import { changeSelection } from './data.actions';

export interface DataState {
  value: string | number | undefined;
  values: Record<string, any[]>;
}

export const initialState: DataState = {
  value: undefined,
  values: {
    a: [{ id: 1, name: 'Rafael' }, { id: 2, name: 'Hugo' }, { id: 3, name: 'André' }],
    b: [{ id: 1, type: 'dog', name: 'Rufus' }, { id: 2, type: 'cat', name: 'Medf' }],
    c: [{ id: 'a', total: 200 }],
  }
};

export const dataReducer = createReducer(
  initialState,
  on(changeSelection, (state, { value }) => ({ ...state, value }))
);
