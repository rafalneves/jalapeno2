import { createSelector } from '@ngrx/store';
import { DataState } from './data.reducer';
import { AppState } from '../app.state';

export const getDataKeys = createSelector(
  (state: AppState) => state.data,
  (data: DataState) => Object.keys(data.values),
);

export const getDataValues = createSelector(
  (state: AppState) => state.data,
  (data: DataState) => Object.values(data.values[data.value || ''] || {}),
);
