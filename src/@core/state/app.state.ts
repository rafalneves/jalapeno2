import { DataState } from './data/data.reducer';

export interface AppState {
  data: DataState;
}
