import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { DropDowOptions } from 'src/@jcomponents/types';
import { getDataKeys, getDataValues } from 'src/@core/state/data/data.selectors';
import { changeSelection } from 'src/@core/state/data/data.actions';
import { AppState } from 'src/@core/state/app.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'milestone-0001-components',
  templateUrl: './milestone-0001.component.html',
})

export class Milestone0001Component implements OnInit {
  public valueSelection: DropDowOptions = [];
  public value: string | number | undefined;
  public dataSelection: Observable<any[]>;

  constructor(private store: Store<AppState>) {
    this.store.pipe(select(getDataKeys))
      .subscribe((values) => {
        this.valueSelection = values.map((value) => ({
          value,
          label: value,
        }));
      });
    this.dataSelection = this.store.pipe(select(getDataValues));

    this.onSelectionChange = this.onSelectionChange.bind(this);
  }

  ngOnInit() {
  }

  public onSelectionChange(event: any) {
    this.value = event.target.value;
    this.store.dispatch(changeSelection({ value: this.value }));
  }
}
