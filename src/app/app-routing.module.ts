import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components/components.component';
import { Milestone0001Component } from 'src/widgets/samples/milestone-0001/milestone-0001.component';

const routes: Routes = [
  { path: 'components', component: ComponentsComponent },
  { path: 'milestone-0001', component: Milestone0001Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
