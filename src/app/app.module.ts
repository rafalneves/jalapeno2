import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ComponentsComponent } from './components/components.component';
import { Milestone0001Component } from 'src/widgets/samples/milestone-0001/milestone-0001.component';

import { JButtonComponent } from 'src/@jcomponents/button/button.component';
import { JDropdownComponent } from 'src/@jcomponents/dropdown/dropdown.component';
import { JDatagridComponent } from 'src/@jcomponents/datagrid/datagrid.component';

import { dataReducer } from 'src/@core/state/data/data.reducer';

@NgModule({
  imports: [
    BrowserModule,
    StoreModule.forRoot({ data: dataReducer }),
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    ComponentsComponent,
    Milestone0001Component,

    JButtonComponent,
    JDropdownComponent,
    JDatagridComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
