import { Component, Injectable, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'j-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.scss'],
})

export class JDatagridComponent implements OnInit {
  @Input() gridData?: Observable<any[]>;
  @Input() staticData?: any[] = [];

  public data: any[] = [];

  public columns: string[] = [];
  public values: string[] = [];

  ngOnInit() {
    console.log(this.staticData, this.gridData)

    if (this.gridData) {
      this.gridData.subscribe((data) => {
        this.data = data;

        this.columns = Object.keys((this.data || [])[0] || {});
        this.values = this.data || [];
      });
    } else {
      this.data = this.staticData || [];
      this.columns = Object.keys((this.data || [])[0] || {});
      this.values = this.data || [];
    }
  }
}
