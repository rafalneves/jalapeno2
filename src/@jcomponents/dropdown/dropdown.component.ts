import { Component, Input } from '@angular/core';

import { DropDowOptions } from '../types';

@Component({
  selector: 'j-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})

export class JDropdownComponent {
  @Input() options: DropDowOptions = [];
  @Input() onChange?: (event: Event) => void;
}
