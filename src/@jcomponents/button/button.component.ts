import { Component, Input } from '@angular/core';

@Component({
  selector: 'j-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})

export class JButtonComponent {
  @Input() label = '';
}
