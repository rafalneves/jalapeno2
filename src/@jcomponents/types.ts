export interface DropDowOption {
  value: string | number;
  label: string;
}

export type DropDowOptions = DropDowOption[];
