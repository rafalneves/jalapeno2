import cors from 'cors';
import express from 'express';
import { middleware } from 'express-openapi-validator';
import http from 'http';
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';

import widgetRouter from './routes/widgets';

const swaggerSpec = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'Sample API',
      version: '0.0.1',
    },
    openapi: '3.0.1',
  },
  apis: [`${__dirname}/schemas/*`, `${__dirname}/routes/*`],
});

const app = express();

const corsOptions = {
  origin(origin, callback) {
    callback(null, true)
  },
  credentials: true,
};

app.use(cors(corsOptions));

app.options('*', cors(corsOptions));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/api-docs.json', (req, res) => res.json(swaggerSpec));

app.use(
  middleware({
    apiSpec: swaggerSpec,
    validateRequests: true,
    validateResponses: true,
  })
);

// disable 304 responses
app.use((req, res, next) => {
  req.headers['if-none-match'] = 'no-match-for-this';
  next();
});

app.use('/widgets', widgetRouter);

app.use((err, req, res, next) => {
  res
    .status(500)
    .json({ err });
});

const PORT = 4201;

const server = http.createServer(app);
server.listen(PORT);
server.on('error', console.error);
server.on('listening', () => console.log(`Listening on ${PORT}`));