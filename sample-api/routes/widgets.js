import { Router } from 'express';

const widgetsRouter = new Router();

/**
 * @swagger
 * /widgets/:
 *   get:
 *     tags:
 *       - getWidgets
 *     operationId: getWidgets
 *     description: Gets widgets
 *     security:
 *       - CookieAuth: []
 *     responses:
 *       '200':
 *         description: Successful response
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 required:
 *                  - name
 *                 properties:
 *                   name:
 *                     type: string
 */
widgetsRouter.get('/', (req, res) => {
	res.status(200).json([{ name: 'widget1' }]);
});

export default widgetsRouter;
